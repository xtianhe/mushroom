var s_HelloWorld = "res/HelloWorld.png";
var s_CloseNormal = "res/CloseNormal.png";
var s_CloseSelected = "res/CloseSelected.png";
var s_Forest = "res/forest1.jpg";
var s_Mushroom = "res/mushroom.png";
var s_Bear_eyeopen = "res/bear_eyesopen.png";
var s_Bear_eyeclose = "res/bear_eyesclosed.png";
var s_Acorn = "res/acorn.png";
var s_Start_button = "res/start_button.png" ;



var g_resources = [
    //image
    {src:s_HelloWorld},
    {src:s_CloseNormal},
    {src:s_CloseSelected},
    {src:s_Forest},
    {src:s_Mushroom} ,
    {src:s_Bear_eyeopen} ,
    {src:s_Bear_eyeclose},
    {src:s_Acorn},
    {src:s_Start_button}

    //plist

    //fnt

    //tmx

    //bgm

    //effect
];