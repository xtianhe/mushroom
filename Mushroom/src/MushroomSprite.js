/**
 * Created with JetBrains WebStorm.
 * User: apple
 * Date: 13-8-21
 * Time: 下午11:47
 * To change this template use File | Settings | File Templates.
 */

var PADDLE_STATE_GRABBED = 0;
var PADDLE_STATE_UNGRABBED = 1;


var MushroomSprite =  cc.Sprite.extend(
{
    _state:PADDLE_STATE_UNGRABBED,
    _rect:null,
    radius : 40,

    rect:function () {
        return cc.rect(-this._rect.width / 2, -this._rect.height / 2, this._rect.width, this._rect.height);
    },
    ctor:function ()
        {
                this._super();
                this.initWithFile(s_Mushroom) ;

        } ,
    onEnter:function()
    {
        cc.Director.getInstance().getTouchDispatcher().addTargetedDelegate(this, 0, true);
        this._super();


    },
    onExit:function()
    {
        cc._directory.getInstance().getTouchDispatcher().removeDelegate(this) ;
        this._super();
    },

    containsTouchLocation:function (touch) {
        var getPoint = touch.getLocation();
        var myRect = this.rect();

        myRect.x += this.getPosition().x;
        myRect.y += this.getPosition().y;
        return cc.rectContainsPoint(myRect, getPoint);//this.convertTouchToNodeSpaceAR(touch));
    },

    onTouchBegan:function (touch, event) {
        if (this._state != PADDLE_STATE_UNGRABBED) return false;
        if (!this.containsTouchLocation(touch)) return false;

        this._state = PADDLE_STATE_GRABBED;
        return true;
    },
    onTouchMoved:function (touch, event) {
        // If it weren't for the TouchDispatcher, you would need to keep a reference
        // to the touch from touchBegan and check that the current touch is the same
        // as that one.
        // Actually, it would be even more complicated since in the Cocos dispatcher
        // you get CCSets instead of 1 UITouch, so you'd need to loop through the set
        // in each touchXXX method.
        cc.Assert(this._state == PADDLE_STATE_GRABBED, "Paddle - Unexpected state!");

        var touchPoint = touch.getLocation();
        //touchPoint = cc.Director.getInstance().convertToGL( touchPoint );

        this.setPosition(cc.p(touchPoint.x, this.getPosition().y));
    },
    onTouchEnded:function (touch, event) {
        cc.Assert(this._state == PADDLE_STATE_GRABBED, "Paddle - Unexpected state!");
        this._state = PADDLE_STATE_UNGRABBED;
    },
    touchDelegateRetain:function () {
    },
    touchDelegateRelease:function () {
    }


 }
);