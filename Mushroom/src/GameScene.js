/**
 * Created with JetBrains WebStorm.
 * User: apple
 * Date: 13-8-21
 * Time: 下午9:43
 * To change this template use File | Settings | File Templates.
 */

var g_GameZOder = {bg: 0, ui: 1,front:100};
var g_GameStatus={normal:0,stop:1,gameOver:2};

var GameLayer = cc.Layer.extend(
    {
        mushroomSprite : null,
        bearSprite :null,
        acornList : [],
        gameStatus : g_GameStatus.stop,
        btnStart: null,
        winLabel : null,
        loseLabel: null,

       ctor:function() {

           this._super();
           //cc.associateWithNative(this,cc.Layer) ;

           this.schedule(this.update,0) ;

           var bg = cc.Sprite.create(s_Forest);

           bg.setAnchorPoint(cc.p(0,0)) ;
           bg.setPosition(cc.p(0,0));
           this.addChild(bg);

           //add mushroom
           this.mushroomSprite = new MushroomSprite();

           this.mushroomSprite.setAnchorPoint(cc.p(0.5,0));
           this.mushroomSprite.setPosition(cc.p(240,0));
           this.addChild(this.mushroomSprite);

           this.bearSprite = new BearSprite();
           this.bearSprite.currentPlayer = this;

           //bearSprite.setAnchorPoint();
           this.bearSprite.setPosition(cc.p(240,60));
           this.addChild(this.bearSprite,g_GameZOder.ui);
           this.bearSprite.beginRotate();

           this.initAcorn();

           //开始按钮
           var start1 = cc.Sprite.create(s_Start_button);
           var start2 = cc.Sprite.create(s_Start_button);

           this.btnStart = cc.MenuItemSprite.create(start1, start2, this.startGame, this);

           var infoMenu = cc.Menu.create(this.btnStart);
           this.addChild(infoMenu, g_GameZOder.front);


           var s = cc.Director.getInstance().getWinSize();
//
//           var label = cc.LabelBMFont.create("G", "res/bitmapFontTest5.fnt");
//           this.addChild(label);
//           label.setPosition(cc.p(s.width / 2, 3 * s.height / 4));
//           label.setAnchorPoint(cc.p(0.5, 0.5));
//           label.setColor(cc.c3b(0, 255, 0));
//           label.setString("Green");

           //cc.log()
           var greenColor = new cc.Color3B(0,255,0);
           this.winLabel = cc.LabelTTF.create("You Win!","Arial",30);
           this.winLabel.setPosition(cc.p(s.width/2, s.height/3));
           this.winLabel.setColor(greenColor);
           //this.winLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
           this.addChild(this.winLabel);
           this.winLabel.setVisible(false);


           var redColor = new cc.Color3B(255,0,0);
           this.loseLabel = cc.LabelTTF.create("You Lose!","Arial",30);
           this.loseLabel.setPosition(cc.p(s.width/2, s.height/3));
            this.loseLabel.setColor(redColor);
           this.addChild(this.loseLabel,g_GameZOder.ui);
           this.loseLabel.setVisible(false);

       },


        initAcorn: function () {
            var left = 0;
            var space = 30;
            for (var i = 1; i <= 15; i++) {
                //添加15个
                var prize = new AcornSprite();

                prize.setPosition(cc.p(left + i * space, 270));
                this.addChild(prize, g_GameZOder.ui);
                this.acornList.push(prize);
            }
        },

        startGame: function () {
           this.winLabel.setVisible(false);
            this.loseLabel.setVisible(false);
            if(this.gameStatus == g_GameStatus.gameOver)
            {
                this.restartGame();

                return;
            }
            this.gameStatus = g_GameStatus.normal;
            this.bearSprite.beginRotate();
            this.btnStart.setVisible(false);
        },
        overGame:function(){
            this.loseLabel.setVisible(true);

            this.gameStatus = g_GameStatus.gameOver;
            //this.bearSprite.stopRotate();
            this.mushroomSprite.setPosition(cc.p(240, 0));
            this.bearSprite.setPosition(cc.p(240,60));
            this.btnStart.setVisible(true);
        },

        winGame:function(){
            this.winLabel.setVisible(true);

            this.gameStatus = g_GameStatus.gameOver;
            //this.bearSprite.stopRotate();
            this.mushroomSprite.setPosition(cc.p(240, 0));
            this.bearSprite.setPosition(cc.p(240,60));
            this.btnStart.setVisible(true);
        },

        restartGame : function()
        {

            this.gameStatus = g_GameStatus.normal;

            for (var i = 0; i < this.acornList.length; i++) {
                var prize = this.acornList[i];

                        prize.setVisible(true);
                        prize.isHit = false;

            }

            this.startGame();

        },

        update: function (dt) {

            if(this.gameStatus != g_GameStatus.normal)
            {
                return;
            }
            this.bearSprite.update(dt) ;
            this.bearSprite.collide(this.mushroomSprite);
             var length = this.acornList.length;

            for (var i = 0; i < this.acornList.length; i++) {
                var prize = this.acornList[i];

                if (!prize.isHit) {
                    if (this.bearSprite.collide(prize)) {
                        prize.setVisible(false);
                        prize.isHit = true;
                        length -= 1;

                    }
                }else
                {
                    length -= 1;
                }
            }

            if( length <= 0)
            {
                 this.winGame();

            }
        }


    }
);

var GameScene = cc.Scene.extend(
    {
       ctor:function(){
           this._super();
           //cc.associateWithNative(this,cc.Scene);

           var layer = new GameLayer();
           this.addChild(layer);

       }
    }
);